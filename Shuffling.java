
import java.util.ArrayList;

public class Shuffling {

public static void main(String[] args) {
	ArrayList<Integer> list = new ArrayList<Integer>();
	list.add(1);
	list.add(2);
	list.add(3);
	list.add(4);
	list.add(5);
	list.add(6);
	list.add(7);
	list.add(8);
	list.add(9);
	list.add(10);

	System.out.println("List of numbers before shuffle:");
	for (int i = 0; i < list.size(); i++)
	System.out.print(list.get(i) + " ");

	shuffle(list);

	System.out.println("\n\nList of numbers after shuffle:");
	for (int i = 0; i < list.size(); i++)
	System.out.print(list.get(i) + " ");

	}

public static <E> void shuffle(ArrayList<E> list) {
	for (int i = 0; i < list.size(); i++) {
	int index = (int)(Math.random() * list.size());
	E o = list.get(i);
	list.set(i, list.get(index));
	list.set(index, o);

	}
	}

}
